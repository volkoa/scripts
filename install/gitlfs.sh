#/bin/bash
# install git lfs:

pushd /tmp
wget https://github.com/git-lfs/git-lfs/releases/download/v1.2.1/git-lfs-linux-amd64-1.2.1.tar.gz
tar xvf git-lfs-linux-amd64-1.2.1.tar.gz
pushd git-lfs-1.2.1
sudo ./install.sh
popd
rm -rf git-lfs-1.2.1
rm -rf git-lfs-linux-amd64-1.2.1.tar.gz
popd

