#!/bin/bash
# functions for virtualenv

[[ "${BASH_SOURCE[0]}" == "${0}" ]] && \
  echo Should be run as : source "${0}" && exit 1

enablevenvglobalsitepackages() {
    if ! [ -z ${VIRTUAL_ENV+x} ]; then
        _libpypath=$(dirname $(python -c \
  "from distutils.sysconfig import get_python_lib; print(get_python_lib())"))
       if ! [[ "${_libpypath}" == *"$VIRTUAL_ENV"* ]]; then
          return # VIRTUAL_ENV path not in the right place
       fi
       no_global_site_packages_file=${_libpypath}/no-global-site-packages.txt
       if [ -f $no_global_site_packages_file ]; then
           rm $no_global_site_packages_file;
           echo "Enabled global site-packages"
       else
           echo "Global site-packages already enabled"
       fi
    fi
}

disablevenvglobalsitepackages() {
    if ! [ -z ${VIRTUAL_ENV+x} ]; then
        _libpypath=$(dirname $(python -c \
  "from distutils.sysconfig import get_python_lib; print(get_python_lib())"))
       if ! [[ "${_libpypath}" == *"$VIRTUAL_ENV"* ]]; then
          return # VIRTUAL_ENV path not in the right place
       fi
       no_global_site_packages_file=${_libpypath}/no-global-site-packages.txt
       if ! [ -f $no_global_site_packages_file ]; then
           touch $no_global_site_packages_file
           echo "Disabled global site-packages"
       else
           echo "Global site-packages were already disabled"
       fi
    fi
}

