#!/bin/bash

dname=${USER}_keras

nvidia-docker run --name=$dname -d -t \
  -u $(id -u):$(id -g) -e HOME=$HOME -e USER=$USER -v $HOME:$HOME \
  nvcr.io/nvidia/tensorflow:17.05

docker exec -it -u root $dname \
  bash -c 'apt-get update && apt-get install -y virtualenv virtualenvwrapper'

docker exec -it $dname \
  bash -c 'source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
  pip install --upgrade pip
  mkvirtualenv py-keras
  pip install keras --no-deps
  pip install PyYaml
  pip install numpy
  pip install ipython'

docker exec -it $dname bash

docker stop $dname && docker rm $dname

